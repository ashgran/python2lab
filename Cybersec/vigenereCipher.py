LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def main():
   
   # myMessage = """Alan Mathison Turing was a British mathematician, logician, cryptanalyst, and computer scientist."""
   # myMessage = """Adiz Avtzqeci Tmzubb wsa m Pmilqev halpqavtakuoi, lgouqdaf, kdmktsvmztsl, izr xoexghzr kkusitaaf."""
    myKey = 'stability'
    myMode = 'encrypt' # Set to either 'encrypt' or 'decrypt'.
    #myMode = 'decrypt' # Set to either 'encrypt' or 'decrypt'.

    if myMode == 'encrypt':
        file = open("msg.txt", "r") 
        myMessage = file.read()
        translated = encryptMessage(myKey, myMessage)
        file.close()
    elif myMode == 'decrypt':
        file = open("cipherMsg.txt", "r") 
        myMessage = file.read()
        translated = decryptMessage(myKey, myMessage)
        file.close()


    print('%sed message:' % (myMode.title()))
    filewrite = open("cipherMsg.txt", "w")
    filewrite.write(translated) 
    filewrite.close()
    
    print(translated)


def encryptMessage(key, message):
    return translateMessage(key, message, 'encrypt')


def decryptMessage(key, message):
    return translateMessage(key, message, 'decrypt')


def translateMessage(key, message, mode):
    translated = [] # Stores the encrypted/decrypted message string.

    keyIndex = 0
    key = key.upper()

    for symbol in message: # Loop through each symbol in message.
        num = LETTERS.find(symbol.upper())
        if num != -1: # -1 means symbol.upper() was not found in LETTERS.
            if mode == 'encrypt':
                num += LETTERS.find(key[keyIndex]) # Add if encrypting.
            elif mode == 'decrypt':
                num -= LETTERS.find(key[keyIndex]) # Subtract if decrypting.

            num %= len(LETTERS) # Handle any wraparound.

            # Add the encrypted/decrypted symbol to the end of translated:
            if symbol.isupper():
                translated.append(LETTERS[num])
            elif symbol.islower():
                translated.append(LETTERS[num].lower())

            keyIndex += 1 # Move to the next letter in the key.
            if keyIndex == len(key):
                keyIndex = 0
        else:
            # Append the symbol without encrypting/decrypting.
            translated.append(symbol)

    return ''.join(translated)


# If vigenereCipher.py is run (instead of imported as a module) call
# the main() function.
if __name__ == '__main__':
    main()
