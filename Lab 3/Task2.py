#!/usr/bin/python

""" Wykonać następujące formatowanie ciągu znaków:

INPUT:    -> OUTPUT:
"Test"    -> "T-Ee-Sss-Tttt"
"MyPa55"  -> "M-Yy-Ppp-Aaaa-55555-555555"
"How's?"  -> "H-Oo-Www-''''-Sssss-??????"

 """


task2 = lambda inputstr: print(inputstr, "  -> ",
                               "-".join(ch.upper() + ch.lower() * (inputstr.find(ch)) for ch in inputstr))



task2("Test")
task2("MyPa55")
task2("How's?")
