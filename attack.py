message = 'HelloWorld'#CODE_IT
LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

for key in range(len(LETTERS)): #CODE_IT
    translated = ''
    message= message.upper();
    for symbol in message:
        if symbol in LETTERS:
            # get the encrypted (or decrypted) number for this symbol
            num = LETTERS.find(symbol) #CODE_IT
            num = num - key

            if num < 0:
                num = num + len(LETTERS)
            # add encrypted/decrypted number's symbol at the end of translated
            translated = translated + LETTERS[num] #CODE_IT
        else:
            translated = translated + symbol

    print('Key #%s: %s' % (key, translated))
