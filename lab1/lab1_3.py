#Zad 3

'''
Napisać program do obsługi podstawowych operacji matematycznych.

Przykładowa funkcja może korzystać z trzech argumentów:
działanie: str
wartość 1: int
wartość 2: int

'''

def basic_op(op,num1,num2):
    if(op == '+'):
        retVal = num1+num2
    if(op == '-'):
        retVal=num1-num2
    if(op == '*'):
        retVal = num1*num2
    if(op == '/'):
         retVal = num1/num2       
    print(retVal)


if __name__ == "__main__":

    basic_op('+', 4, 7)      
    basic_op('-', 15, 18)    
    basic_op('*', 5, 5)   
    basic_op('/', 49, 7)     


