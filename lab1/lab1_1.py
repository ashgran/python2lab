#Zad 1

'''
Napisać funkcję usuwającą spację w podanym ciągu znaków.

Wykorzystane zmienne:
test_string : ciąg znaków
no_space    : zwracana wartość bez spacji
'''



test_string = "Hey! Do you have anything to say? ? ?"
print (test_string)


no_space = test_string.replace(" ","")
print (no_space)
